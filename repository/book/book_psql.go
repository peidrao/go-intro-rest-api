package bookRepository

import (
	"books-list/models"
	"database/sql"
)

type BookRepository struct{}

func (b BookRepository) GetBooks(db *sql.DB, book models.Book, books []models.Book) ([]models.Book, error) {
	selDB, err := db.Query("SELECT * FROM books")
	if err != nil {
		return []models.Book{}, err
	}

	for selDB.Next() {
		err = selDB.Scan(&book.ID, &book.Title, &book.Author, &book.Year)
		books = append(books, book)
	}

	if err != nil {
		return []models.Book{}, err
	}

	return books, nil
}

func (b BookRepository) GetBook(db *sql.DB, book models.Book, id int) (models.Book, error) {
	row := db.QueryRow("SELECT * FROM books WHERE id=$1", id)
	err := row.Scan(&book.ID, &book.Title, &book.Author, &book.Year)

	return book, err
}

func (b BookRepository) AddBook(db *sql.DB, book models.Book) (int, error) {
	err := db.QueryRow("INSERT INTO books (title, author, year) values($1, $2, $3) RETURNING id;",
		book.Title, book.Author, book.Year).Scan(&book.ID)

	if err != nil {
		return 0, err
	}
	return book.ID, nil
}

func (b BookRepository) UpdateBook(db *sql.DB, book models.Book, id int) (int64, error) {
	result, err := db.Exec("UPDATE books SET title=$1, author=$2, year=$3 WHERE id=$4 RETURNING id",
		book.Title, book.Author, book.Year, id)
	if err != nil {
		return 0, err
	}

	rowsUpdate, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}
	return rowsUpdate, nil
}

func (b BookRepository) DeleteBook(db *sql.DB, id int) (int64, error) {
	query, err := db.Exec("DELETE FROM books WHERE id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsDeleted, err := query.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsDeleted, nil
}
