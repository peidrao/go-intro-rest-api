module books-list

go 1.13

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.6
	github.com/subosito/gotenv v1.2.0
)
