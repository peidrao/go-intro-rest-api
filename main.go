package main

import (
	"books-list/controllers"
	"books-list/driver"
	"fmt"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/subosito/gotenv"
)

func init() {
	gotenv.Load()
}

func main() {
	db := driver.DatabaseConnect()
	router := mux.NewRouter()
	controller := controllers.Controller{}
	router.HandleFunc("/books/{id}", controller.GetBook(db)).Methods("GET")
	router.HandleFunc("/create", controller.AddBook(db)).Methods("POST")
	router.HandleFunc("/books", controller.GetBooks(db))
	router.HandleFunc("/books/{id}", controller.UpdateBook(db)).Methods("PUT")
	router.HandleFunc("/books/{id}", controller.DeleteBook(db)).Methods("DELETE")

	http.Handle("/", router)
	fmt.Println("Running Server in port 8000")
	http.ListenAndServe(":8000", handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}))(router))
}
